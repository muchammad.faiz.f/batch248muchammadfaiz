﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.ViewModel;
using XPOS.Model;
using static XPos.ViewModel.ResponseResult;

namespace XPos.Repository
{
    public class OrderRepo
    {
        public static ResultOrder Post(OrderHeaderViewModel entity)
        {
            ResultOrder result = new ResultOrder();
            try
            {
                using (var db = new XPosContext())
                {
                    string newReference = NewReference();

                    OrderHeader orderHeader = new OrderHeader();
                    orderHeader.Reference = newReference;
                    orderHeader.Amount = entity.Amount;
                    orderHeader.Active = true;

                    orderHeader.CreatedBy = "Faiz";
                    orderHeader.CreatedDate = DateTime.Now;

                    db.OrderHeaders.Add(orderHeader);

                    foreach (var item in entity.Details)
                    {
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.HeaderId = orderHeader.Id;
                        orderDetail.ProductId = item.ProductId;
                        orderDetail.Price = item.Price;
                        orderDetail.Quantity = item.Quantity;
                        orderDetail.Active = true;

                        orderDetail.CreatedBy = "Rizal";
                        orderDetail.CreatedDate = DateTime.Now;

                        Product product = db.Products.Where(o => o.Id == orderDetail.ProductId).FirstOrDefault();
                        

                        if (product.Stock < orderDetail.Quantity)
                        {
                            result.Success = false;
                            result.Message = "Your quantity of order is greater than the Stock!";
                            break;
                        }
                        else
                        {
                            product.Stock -= orderDetail.Quantity;
                        }

                        db.OrderDetails.Add(orderDetail);
                    }

                    db.SaveChanges();

                    result.Reference = newReference;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        public static String NewReference()
        {
            string yearMonth = DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2");
            string result = "SLS-" + yearMonth + "-";

            using (var db = new XPosContext())
            {
                var maxReference = db.OrderHeaders
                    .Where(oh => oh.Reference.Contains(result))
                    .Select(oh => new { reference = oh.Reference })
                    .OrderByDescending(oh => oh.reference)
                    .FirstOrDefault();

                if (maxReference != null)
                {
                    string[] oldReference = maxReference.reference.Split('-');
                    int newIncrement = int.Parse(oldReference[2]) + 1;
                    result += newIncrement.ToString("D4");
                }
                else
                {
                    result += "0001";
                }
            }
            return result;
        }

    }
}


//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using XPos.ViewModel;
//using XPOS.Model;
//using static XPos.ViewModel.ResponseResult;

//namespace XPosRepository
//{
//    public class OrderRepo
//    {
//        public static ResultOrder Post (OrderHeaderViewModel entity)
//        {
//            ResultOrder result = new ResultOrder();
//            try
//            {
//                using (var db = new XPosContext())
//                {
//                    string newReference = NewReference();
//                    OrderHeader orderHeader = new OrderHeader();
//                    orderHeader.Reference = newReference;
//                    orderHeader.Amount = entity.Amount;
//                    orderHeader.Active = true;

//                    orderHeader.CreatedBy = "zaipin";
//                    orderHeader.CreatedDate = DateTime.Now;

//                    db.OrderHeaders.Add(orderHeader);

//                    foreach (var item in entity.Details)
//                    {
//                        OrderDetail orderDetail = new OrderDetail();
//                        orderDetail.HeaderId = orderDetail.Id;
//                        orderDetail.ProductId = item.ProductId;
//                        orderDetail.Price = item.Price;
//                        orderDetail.Quantity = item.Quantity;
//                        orderDetail.Active = true;

//                        orderDetail.CreatedBy = "zaipin2";
//                        orderDetail.CreatedDate = DateTime.Now;

//                        db.OrderDetails.Add(orderDetail);
//                    }
//                    db.SaveChanges();

//                    result.Reference = newReference;

//                }

//            }
//            catch (Exception ex)
//            {
//                result.Success = false;
//                result.Message = ex.Message;
//            }
//            return result;
//        }


//        public static string NewReference()
//        {
//            string yearMonth = DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2");
//            string result = "SLS-" + yearMonth + "-";

//            using (var db = new XPosContext())
//            {
//                var maxReference = db.OrderHeaders
//                    .Where(oh => oh.Reference.Contains(result))
//                    .Select(oh => new { reference = oh.Reference })
//                    .OrderByDescending(oh => oh.reference)
//                    .FirstOrDefault();

//                if (maxReference != null)
//                {
//                    string[] oldReference = maxReference.reference.Split('_');
//                    int newIncrement = int.Parse(oldReference[2]) + 1;
//                    result += newIncrement.ToString("D4");
//                }
//                else
//                {
//                    result += "0001";
//                }
//            }
//            return result;
//        }
//    }
//}
