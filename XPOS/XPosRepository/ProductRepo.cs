﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.ViewModel;
using XPOS.Model;

namespace XPosRepository
{
    public class ProductRepo
    {
        //// Get All
        //public static List<ProductViewModel> GetAll()
        //{
        //    //List<ProductViewModel> result = new
        //    //    List<ProductViewModel>();

        //    //using (var db = new XPosContext())
        //    //{
        //    //    result = (from cat in db.Products
        //    //              select new ProductViewModel
        //    //              {
        //    //                  Id = cat.Id,
        //    //                  Initial = cat.Initial,
        //    //                  Name = cat.Name,
        //    //                  Active = cat.Active,
        //    //              }).ToList();
        //    //}
        //    return ByVariant (-1);
        //}
        // Get All
        public static Tuple<List<ProductViewModel> ,int >GetAll(int page, int count)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int rowNumber = 0;

            using (var db = new XPosContext())
            {
                var query = db.Products;

                rowNumber = query.Count();
                result = query
                    .OrderBy(p => p.Id)
                    .Skip((page - 1)*count)
                    .Take(count)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Active = c.Active,
                        Stock = c.Stock

                    })
                    .ToList();
                return new Tuple<List<ProductViewModel>, int>(result, rowNumber);
            }
        }

        public static Tuple<List<ProductViewModel>, int> GetByFilter (string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int count = 0;

            using (var db = new XPosContext())
            {
                var query = db.Products
                    .Where(p => p.Initial.Contains(search) || p.Name.Contains(search) || p.Description.Contains(search));
                count = query.Count();
                result = query
                    .Take(5)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Active = c.Active,
                        Stock = c.Stock

                    })
                    .ToList();

            }
            return new Tuple<List<ProductViewModel>, int>(result, count);
        }

        public static List<ProductViewModel> ByVariant(long id)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(v => v.VariantId == (id == -1 ? v.VariantId : id))
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Price = c.Price,
                        Description = c.Description,
                        Active = c.Active,
                        Stock = c.Stock
                    }).ToList();
            }
            return result;
        }


        // Get by id
        public static ProductViewModel GetbyId(int id)
        {
            ProductViewModel result = new ProductViewModel();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Products
                          where cat.Id == id
                          select new ProductViewModel
                          {
                              Id = cat.Id,
                              CategoryId = cat.Variant.CategoryId,
                              CategoryName = cat.Variant.Category.Name,
                              VariantId = cat.VariantId,
                              VariantName = cat.Variant.Name,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active,
                              Description = cat.Description,
                              Price = cat.Price,
                              Stock = cat.Stock
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }

        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create new / Insert
                    if (entity.Id == 0)
                    {
                        Product Product = new Product();

                        Product.Id = entity.Id;
                        Product.VariantId = entity.VariantId;
                        Product.Initial = entity.Initial;
                        Product.Name = entity.Name;
                        Product.Active = entity.Active;
                        Product.Description = entity.Description;
                        Product.Price = entity.Price;
                        Product.Stock = entity.Stock;

                        Product.CreatedBy = "Faiz";
                        Product.CreatedDate = DateTime.Now;

                        db.Products.Add(Product);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Product Product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();
                        if (Product != null)
                        {
                            Product.VariantId = entity.VariantId;
                            Product.Initial = entity.Initial;
                            Product.Name = entity.Name;
                            Product.Active = entity.Active;
                            Product.Description = entity.Description;
                            Product.Price = entity.Price;
                            Product.Stock = entity.Stock;

                            Product.ModifiedBy = "Faiz";
                            Product.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Product is not found";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Product Product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();
                    if (Product != null)
                    {
                        result.Entity = entity;
                        db.Products.Remove(Product);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Product is not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }

        //public static List<ProductViewModel> GetBySearch (string search)
        //{
        //    List<ProductViewModel> result = new List<ProductViewModel>();
        //    using (var db = new XPosContext())
        //    {
        //        result = db.Products
        //            .Where(o => o.Active == true && (o.Initial.Contains(search) || o.Name.Contains(search) || o.Description.Contains(search)))
        //            .Take(10)
        //            .Select(p => new ProductViewModel
        //            {
        //                Id = p.Id,
        //                VariantId = p.VariantId,
        //                VariantName = p.Variant.Name,
        //                CategoryName = p.Variant.Category.Name,
        //                Initial = p.Initial,
        //                Name = p.Name,
        //                Description = p.Description,
        //                Price = p.Price,
        //                Active = p.Active
        //            }).ToList();
        //    }
        //    return result;
        //}


        public static List<ProductViewModel> GetBySearch(string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.Variant.CategoryId,
                        CategoryName = p.Variant.Category.Name,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stock = p.Stock
                    }).ToList();
            }
            return result;
        }

        //Order By Ascending
        public static List<ProductViewModel> GetByAscending(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .OrderBy(o => o.Name)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.Variant.CategoryId,
                        CategoryName = p.Variant.Category.Name,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stock = p.Stock
                        
                    }).ToList();
            }
            return result;
        }

        //Order By Descending
        public static List<ProductViewModel> GetByDescending(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .OrderByDescending(o => o.Name)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.Variant.CategoryId,
                        CategoryName = p.Variant.Category.Name,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stock = p.Stock
                    }).ToList();
            }
            return result;
        }


    }
}
