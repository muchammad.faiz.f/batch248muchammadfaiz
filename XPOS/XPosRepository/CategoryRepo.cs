﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.ViewModel;
using XPOS.Model;

namespace XPosRepository
{
    public class CategoryRepo
    {

        // Get All
        public static List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          select new CategoryViewModel
                          {
                              id = cat.Id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).ToList();
            }
            return result;
        }

        public static Tuple<List<CategoryViewModel>, int> GetListPagination(int page, int count)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            int rowNumber = 0;

            using (var db = new XPosContext())
            {
                var query = db.Categories;

                rowNumber = query.Count();
                result = query
                    .OrderBy(p => p.Id)
                    .Skip((page - 1) * count)
                    .Take(count)
                    .Select(c => new CategoryViewModel
                    {
                        id = c.Id,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active

                    })
                    .ToList();
                return new Tuple<List<CategoryViewModel>, int>(result, rowNumber);
            }
        }

        // Get by id
        public static CategoryViewModel GetbyId(int id)
        {
            CategoryViewModel result = new CategoryViewModel();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          where cat.Id == id
                          select new CategoryViewModel
                          {
                              id = cat.Id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new CategoryViewModel();
        }

        public static ResponseResult Update (CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create new / Insert
                    if(entity.id == 0)
                    {
                        Category category = new Category();

                        category.Initial = entity.Initial;
                        category.Name = entity.Name;
                        category.Active = entity.Active;

                        category.CreatedBy = "Faiz";
                        category.CreatedDate = DateTime.Now;

                        db.Categories.Add(category);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Category category = db.Categories.Where(o => o.Id == entity.id).FirstOrDefault();
                        if (category != null)
                        {
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;

                            category.ModifiedBy = "Faiz";
                            category.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category is not found";
                        }
                        #endregion
                    }
                }
            }
            catch(Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                
            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Category category = db.Categories.Where(o => o.Id == entity.id).FirstOrDefault();
                    if (category != null)
                    {
                        result.Entity = entity;
                        db.Categories.Remove(category);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category is not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }

        //Get By Search
        public static List<CategoryViewModel> GetBySearch(string search)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Ascending
        public static List<CategoryViewModel> GetByAscending(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .OrderBy(o => o.Name)
                    .Select(p => new CategoryViewModel
                    {
                        id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Descending
        public static List<CategoryViewModel> GetByDescending(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .OrderByDescending(o => o.Name)
                    .Select(p => new CategoryViewModel
                    {
                        id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        public static List<CategoryViewModel> GetActiveList(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == true)
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        public static List<CategoryViewModel> GetNonActiveList(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == false)
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }



    }
}