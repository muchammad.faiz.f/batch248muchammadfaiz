﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.ViewModel;
using XPOS.Model;

namespace XPosRepository
{
    public class VariantRepo
    {
        // Get All
        public static List<VariantViewModel> GetAll()
        {
            //List<VariantViewModel> result = new
            //    List<VariantViewModel>();

            //using (var db = new XPosContext())
            //{
            //    result = (from cat in db.Variants
            //              select new VariantViewModel
            //              {
            //                  id = cat.Id,
            //                  CategoryId = cat.CategoryId,
            //                  CategoryName = cat.Category.Name,
            //                  Initial = cat.Initial,
            //                  Name = cat.Name,
            //                  Active = cat.Active
            //              }).ToList();
            //}
            return ByCategory(-1);
        }


        public static Tuple<List<VariantViewModel>, int> GetListPagination(int page, int count)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            int rowNumber = 0;

            using (var db = new XPosContext())
            {
                var query = db.Variants;

                rowNumber = query.Count();
                result = query
                    .OrderBy(p => p.Id)
                    .Skip((page - 1) * count)
                    .Take(count)
                    .Select(c => new VariantViewModel
                    {
                        id = c.Id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active

                    })
                    .ToList();
                return new Tuple<List<VariantViewModel>, int>(result, rowNumber);
            }
        }

        // Get by id
        public static VariantViewModel GetbyId(int id)
        {
            VariantViewModel result = new VariantViewModel();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Variants
                          where cat.Id == id
                          select new VariantViewModel
                          {
                              id = cat.Id,
                              CategoryId = cat.CategoryId,
                              CategoryName = cat.Category.Name,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new VariantViewModel();
        }

        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create new / Insert
                    if (entity.id == 0)
                    {
                        Variant variant = new Variant();
                        variant.CategoryId = entity.CategoryId;
                        variant.Initial = entity.Initial;
                        variant.Name = entity.Name;
                        variant.Active = entity.Active;


                        variant.CreatedBy = "Faiz";
                        variant.CreatedDate = DateTime.Now;

                        db.Variants.Add(variant);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Variant Variant = db.Variants.Where(o => o.Id == entity.id).FirstOrDefault();
                        if (Variant != null)
                        {
                            
                            Variant.CategoryId = entity.CategoryId;
                            Variant.Initial = entity.Initial;
                            Variant.Name = entity.Name;
                            Variant.Active = entity.Active;

                            Variant.ModifiedBy = "Faiz";
                            Variant.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Variant is not found";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Variant variant = db.Variants.Where(o => o.Id == entity.id).FirstOrDefault();
                    if (variant != null)
                    {
                        result.Entity = entity;
                        db.Variants.Remove(variant);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Variant is not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }

        public static List<VariantViewModel> ByCategory(long id)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(v => v.CategoryId == (id == -1 ? v.CategoryId : id))
                    .Select(c => new VariantViewModel
                    {
                        id = c.Id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    }).ToList();
            }
            return result;
        }

        public static List<VariantViewModel> GetBySearch(string search)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(p => new VariantViewModel
                    {
                        id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Ascending
        public static List<VariantViewModel> GetByAscending(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .OrderBy(o => o.Name)
                    .Select(p => new VariantViewModel
                    {
                        id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Descending
        public static List<VariantViewModel> GetByDescending(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .OrderByDescending(o => o.Name)
                    .Select(p => new VariantViewModel
                    {
                        id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

    }
}
