﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.ViewModel;
using XPosRepository;

namespace XPOS.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, int count = 10)
        {
            var ListProduct = ProductRepo.GetAll(page, count);
            decimal pageTotal = (decimal)ListProduct.Item2 / count;
            int fullPage = ListProduct.Item2 / count;

            if (pageTotal - fullPage > 0)
            {
                fullPage += 1;
            }
            ViewBag.PageTotal = fullPage;
            return PartialView("_List", ListProduct.Item1);
        }

        public ActionResult OrderAscList(string Name)
        {
            return PartialView("_List", ProductRepo.GetByAscending(Name));
        }

        public ActionResult OrderDescList(string Name)
        {
            return PartialView("_List", ProductRepo.GetByDescending(Name));
        }

        public ActionResult ProductList(string search = "")
        {
            return PartialView("_List", ProductRepo.GetBySearch(search));
        }

        public ActionResult ListByVariant(long id = 0)
        {
            return PartialView("_ListByVariant", ProductRepo.ByVariant(id));
        }

        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.GetAll(), "Id", "Name");
            //return PartialView("_Create");
            return PartialView("_Create", new ProductViewModel());
            //return PartialView("_Create", ProductRepo.GetbyId(id));
        }


        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            ProductViewModel model = ProductRepo.GetbyId(id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(model.CategoryId), "id", "Name");
            return PartialView("_Edit", model);

        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            ProductViewModel model = ProductRepo.GetbyId(id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(model.CategoryId), "id", "Name");
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductListOrder (string search = "")
        {
            var result = ProductRepo.GetByFilter(search);
            return PartialView("_ProductListOrder", result.Item1);
        }

    }
}