﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.ViewModel;
using XPosRepository;

namespace XPOS.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult List()
        //{
        //    return PartialView("_List", CategoryRepo.GetAll());
        //}

        public ActionResult List(int page = 1, int count = 10)
        {
            var ListProduct = CategoryRepo.GetListPagination(page, count);
            decimal pageTotal = (decimal)ListProduct.Item2 / count;
            int fullPage = ListProduct.Item2 / count;

            if (pageTotal - fullPage > 0)
            {
                fullPage += 1;
            }
            ViewBag.PageTotal = fullPage;
            return PartialView("_List", ListProduct.Item1);
        }

        public ActionResult OrderAscList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetByAscending(Name));
        }

        public ActionResult OrderDescList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetByDescending(Name));
        }

        public ActionResult SearchList(string search = "")
        {
            return PartialView("_List", CategoryRepo.GetBySearch(search));
        }

        public ActionResult Create()
        {
            return PartialView("_Create", new CategoryViewModel());
        }


        [HttpPost]
        public ActionResult Create(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {

            return PartialView("_Edit",  CategoryRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {

            return PartialView("_Delete", CategoryRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Delete(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

    }
}