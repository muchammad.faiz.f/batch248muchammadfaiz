﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPos.ViewModel
{
    public class ResponseResult
    {
        public ResponseResult()
        {
            Success = true;
        }
        public string Message { get; set; }

        public object Entity { get; set; }
        public bool Success { get; set; }

        public class ResultOrder : ResponseResult
        {
            public string Reference { get; set; }
        }
    }
}
