﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPos.ViewModel
{
    public class ProductViewModel
    {

        public long Id { get; set; }

        [Display(Name = "Variant")]
        public long VariantId { get; set; }       

        [Required]
        [StringLength(10)]
        public string Initial { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }
<<<<<<< HEAD
        public decimal? Stock { get; set; }
=======
        public decimal Stock { get; set; }
>>>>>>> 8dc74c9faf0edf0befff2409bee8a900b0033260


        public decimal Price{ get; set; }

        public bool Active { get; set; }

        [Display(Name = "Variant")]
        public string VariantName { get; set; }

        [Display(Name = "Category")]
        public long CategoryId { get; set; }

        [Display(Name = "Category")]
        public string CategoryName { get; set; }

    }
}
