﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPos.ViewModel
{
    public class OrderHeaderViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(15)]
        public string Reference { get; set; }

        public decimal Amount { get; set; }

        [Display(Name = "Product")]
        public string ProductName { get; set; }

        public List<OrderDetailViewModel> Details { get; set; }

    }

    public class OrderDetailViewModel
    {
        public OrderDetailViewModel()
        {
            Quantity = 1;
        }
        public long Id { get; set; }

        [Display(Name = "Product")]
        public string ProductName { get; set; }

        public long HeaderId { get; set; }

        public long ProductId { get; set; }

        public decimal Quantity { get; set; }
        public decimal ProductStock { get; set; }

        public decimal Price { get; set; }

        public decimal Amount
        {
            get
            {
                return Quantity * Price;
            }
            set { }
        }
    }

}
